all: header.mlt footer.mlt

header.mlt: /etc/hcoop.header header.setTitle header.mlt.in
	cat header.setTitle >header.mlt
	hcoop_header "<% Web.html title %>" >>header.mlt
	cat header.mlt.in >>header.mlt

footer.mlt: /etc/hcoop.footer footer.mlt.in
	cat footer.mlt.in >footer.mlt
	hcoop_footer >>footer.mlt
