structure ChooseDomain :> CHOOSE_DOMAIN = struct

fun domains user =
    let
 	val proc = Unix.execute ("/bin/sh", ["-c", "DOMTOOL_USER=hcoop /usr/local/bin/domtool-admin perms " ^ user])
	val inf = Unix.textInstreamOf proc

	fun loop () =
	    case TextIO.inputLine inf of
		NONE => []
	      | SOME line =>
		case String.tokens (fn ch => ch = #":") line of
		    ["domain", domains] => String.tokens Char.isSpace domains
		  | _ => loop ()
    in
	loop ()
	before ignore (Unix.reap proc)
    end

fun yourDomain {user, domain} = List.exists (fn x => x = domain) (domains user)

end
