signature CONFIG = sig

val scratchDir : string
val urlPrefix : string
val emailSuffix : string
val boardEmail : string
val dbstring : string
val kerberosSuffix : string
val passwordFiles : string

val passgenDbstring : string

end
