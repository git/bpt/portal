signature CONTACT =
sig
    val main : string * string list -> OS.Process.status
end
