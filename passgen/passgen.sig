signature PASSGEN = sig
    structure C : SQL_CLIENT

    val begin : unit -> unit
    val commit : unit -> unit

    val gen : unit -> int * string
end
