signature POLL = sig
    type poll = {id : int, usr : int, title : string, descr : string, starts : string, ends : string, votes : int, official : bool, ready : bool}

    val lookupPoll : int -> poll
    val listPolls : unit -> poll list
    val listCurrentPolls : unit -> poll list
    val listPollsLimit : int -> poll list

    val addPoll : int * string * string * string * string * int * bool * bool -> int
    val modPoll : poll -> unit
    val deletePoll : int -> unit

    type choice = {id : int, pol : int, seq : real, descr : string}

    val lookupChoice : int -> choice
    val addChoice : int * real * string -> int
    val modChoice : choice -> unit
    val deleteChoice : int -> unit
    val listChoices : int -> choice list

    val vote : int * int * int list -> unit

    val dateLe : string * string -> bool
    val dateGeNow : string -> bool
    val dateLeNow : string -> bool
    val dateLtNow : string -> bool

    val canModify : poll -> bool
    val requireCanModify : poll -> unit

    val nextSeq : int -> real
			 
    val takingVotes : poll -> bool
    val listChoicesWithVotes : int -> (bool * int * choice) list
    val listChoicesWithMyVotes : int -> (bool * choice) list

    val noDupes : ''a list -> bool
    val listVoters : int -> Init.user list
    (* This operates on choice IDs. *)

    (* These operate on poll IDs. *)
    val countVoters : int -> int
    val listPollVoters : int -> Init.user list

    val votingMembershipRequirement : int

    val membershipLength : int -> int
end
