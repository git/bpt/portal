signature PREF =
sig
    val hasDirectory : int -> bool
    val setDirectory : int -> unit
    val unsetDirectory : int -> unit

    val subscribed : string * string -> bool
    val subscribe : string * string -> bool
    val unsubscribe : string * string -> bool
end