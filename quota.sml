structure Quota = Request(struct
			  val table = "Quota"
			  val adminGroup = "server"
			  fun subject _ = "Disk quota change request"
			  val template = "quota"
			  val descr = "Quota request"
				      
			  fun body (mail, data) =
			      (Mail.mwrite (mail, "Request: ");
			       Mail.mwrite (mail, data);
			       Mail.mwrite (mail, "\n"))
			  end)
