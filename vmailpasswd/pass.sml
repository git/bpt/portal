structure Pass :> PASS =
struct

fun validEmail email =
    case String.fields (fn ch => ch = #"@") email of
	[_, dom] => dom <> "localhost"
      | _ => false

fun change (email, old, new) =
    let
	val cmd = String.concat ["/usr/bin/sudo /usr/local/bin/vmailpasswd \"",
				 String.toString email,
				 "\" \"",
				 String.toString old,
				 "\" \"",
				 String.toString new,
				 "\" >/dev/null 2>/dev/null"]
    in
	OS.Process.isSuccess (OS.Process.system cmd)
    end

end
